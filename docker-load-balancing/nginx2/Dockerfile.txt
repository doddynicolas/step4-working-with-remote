FROM nginx
RUN sudo ifconfig 10.0.2.17 netmask 255.255.255.0
RUN sudo apt get clean && sudo rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
COPY nginx.conf /etc/nginx/conf.d/default.conf