#!/bin/bash
apt-get update
apt-get install nginx
apt-get install nodejs
apt-get install npm
apt-get install mongoDB
curl https://raw.githubusercontent.com/creationix/nvm/master/install.sh | bash 
source ~/.bashrc
nvm install node
npm install -g @angular/cli
npm install -g @angular/cli@8        
npm install -g @angular/cli@9        
npm install -g @angular/cli@10
apt-get update
ng new hello-world
apt install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
